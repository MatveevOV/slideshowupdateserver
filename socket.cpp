#include "socket.h"

Socket::Socket(QObject *parent) : QTcpSocket(parent)
{
    connect(this, SIGNAL(readyRead()), this, SLOT(readClient()));
    connect(this, SIGNAL(disconnected()), this, SLOT(deleteLater()));
    nextBlockSize = 0;

}

void Socket::readClient()
{
    QDataStream in(this);
    in.setVersion(QDataStream::Qt_4_1);
    if (nextBlockSize == 0) {
        if (bytesAvailable() < sizeof(quint16))
            return;
        in >> nextBlockSize;
    }
    if (bytesAvailable() < nextBlockSize)
        return;
    quint8 requestType;
    in >> requestType;
    if (requestType == 'S')
    {
        generateResponse();
        QDataStream out(this);
        out << quint16(0xFFFF);
    }

    qDebug() << nextBlockSize;

    close();

}

void Socket::generateResponse()
{
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_1);

    out << quint16(0) << QString("Получено");
    out.device()->seek(0);

    out << quint16(block.size() - sizeof(quint16));
    write(block);

    qDebug() << block;
}

