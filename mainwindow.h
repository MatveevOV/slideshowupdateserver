#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QFile>
#include <QDataStream>
#include <QDir>
#include <QSettings>


#include <QSqlDatabase>
#include <QSqlQuery>

#include "quazip/quazip.h"
#include "quazip/quazipfile.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum RequestType {
        SoftVersion,
        Soft,
        DataVersion,
        Data
    };

private:
    Ui::MainWindow *ui;

    QTcpServer* m_ptcpServer;
    quint64     m_nNextBlockSize;

    quint16 softVersion;
    quint16 dataVersion;


private:
    void sendToClient(QTcpSocket* pSocket, const QString& str);
    void sendVersionToClient(QTcpSocket *pSocket, quint8 type, quint16 version);
    void unzipArchive(QString zipName, QString destinationName);

public slots:
    virtual void slotNewConnection();
    void slotReadClient   ();

private slots:
    void on_closeButton_clicked();
    void sendFile(QFile &file, quint8 type, QTcpSocket * socket);
};

#endif // MAINWINDOW_H
