#ifndef SOCKET_H
#define SOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QDataStream>

class Socket : public QTcpSocket

{
    Q_OBJECT

public:
    Socket(QObject *parent = 0);

private slots:
    void readClient();

private:
    void generateResponse();
    quint16 nextBlockSize;

signals:


};

#endif // SOCKET_H
