#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_ptcpServer = new QTcpServer(this);
    if (!m_ptcpServer->listen(QHostAddress::Any, 54256))
    {
        QMessageBox::critical(0,
                              "Server Error",
                              "Unable to start the server:"
                              + m_ptcpServer->errorString()
                             );
        m_ptcpServer->close();
        return;
    }
    else
    {

        QString ipAddress;
        QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
        // use the first non-localhost IPv4 address
        for (int i = 0; i < ipAddressesList.size(); ++i) {
            if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
                ipAddressesList.at(i).toIPv4Address()) {
                ipAddress = ipAddressesList.at(i).toString();
                break;
            }
        }
        // if we did not find one, use IPv4 localhost
        if (ipAddress.isEmpty())
            ipAddress = QHostAddress(QHostAddress::LocalHost).toString();



        ui->statusBar->showMessage(QString("Адрес сервера: %1 Порт: %2").arg(ipAddress)
                                                                      .arg(m_ptcpServer->serverPort()));

    }

    connect(m_ptcpServer, &QTcpServer::newConnection,this,&MainWindow::slotNewConnection);

    QSettings settings (QString ("versions"), QSettings::IniFormat);


    softVersion = settings.value("ActualSoftVersion", 1).value<quint16>();
    dataVersion = settings.value("ActualDataVersion", 1).value<quint16>();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendToClient(QTcpSocket *pSocket, const QString &str)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << str;

    out.device()->seek(0);
    out << quint64(arrBlock.size() - sizeof(quint64));

    pSocket->write(arrBlock);
}

void MainWindow::sendVersionToClient(QTcpSocket *pSocket, quint8 type, quint16 version)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_5);
    out << quint64(0) << type <<version;

    out.device()->seek(0);
    out << quint64(arrBlock.size() - sizeof(quint64));

    qDebug() << "write" << pSocket->write(arrBlock);

}


void MainWindow::slotNewConnection()
{
    QTcpSocket* pClientSocket = m_ptcpServer->nextPendingConnection();
    connect(pClientSocket, SIGNAL(disconnected()), pClientSocket, SLOT(deleteLater()) );
    connect(pClientSocket, SIGNAL(readyRead()), this,SLOT(slotReadClient()));


    qDebug() << pClientSocket;

    //sendToClient(pClientSocket, "Server Response: Connected!");
    m_nNextBlockSize = 0;

}

void MainWindow::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();

    QSettings settings (QString ("versions"), QSettings::IniFormat);

    softVersion = settings.value("ActualSoftVersion", 1).value<quint16>();
    dataVersion = settings.value("ActualDataVersion", 1).value<quint16>();

    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_5_5);
    for (;;)
    {
        if (m_nNextBlockSize == 0) {
            if (pClientSocket->bytesAvailable() < sizeof(quint64)) {
                break;
            }
            in >> m_nNextBlockSize;
        }

        if (pClientSocket->bytesAvailable() < m_nNextBlockSize) {
            break;
        }
        quint8 request;
        in >> request;

        switch (request) {
        case SoftVersion:
        {
            qDebug() << "soft version request";
            sendVersionToClient(pClientSocket,SoftVersion, softVersion);
        }
            break;
        case Soft:
        {
            qDebug() << "soft request";
            QString filePath = QString("./SoftArchive/%1/OphtalmologicExamination.zip").arg(softVersion);
            QFile file(filePath);
            sendFile(file, Soft, pClientSocket);
        }
            break;
        case DataVersion:
        {
            qDebug() << "data version request";
            sendVersionToClient(pClientSocket, DataVersion, dataVersion);

        }
            break;
        case Data:
        {
            QString filePath = QString("./DataArchive/%1/data.zip").arg(dataVersion);
            QFile file(filePath);
            sendFile(file, Data, pClientSocket);
        }
            break;
        default:
            break;
        }

        m_nNextBlockSize = 0;
    }


}

void MainWindow::sendFile(QFile &file, quint8 type, QTcpSocket * socket)
{

    file.open(QFile::ReadOnly);
    int blockFileSize = 1024;

    //отправка размера файла

    QByteArray bArray;
    QDataStream dStream(&bArray, QIODevice::WriteOnly);
    dStream.setVersion(QDataStream::Qt_5_5);

    dStream << type;

    dStream << quint64(file.size());


    QFileInfo fi(file);

    dStream << fi.baseName().toLocal8Bit();

    socket->write(bArray);

    socket->waitForBytesWritten();

    while(!file.atEnd())
    {
        QByteArray ba = file.read(blockFileSize);
        socket->write(ba);
        socket->waitForBytesWritten();
    }

    file.close();

}

void MainWindow::unzipArchive(QString zipName, QString destinationName)
{
    QDir extractDir("./extracted");
    QuaZip zip(zipName);
    if (zip.open(QuaZip::mdUnzip))
    {
        QList<QuaZipFileInfo> list = zip.getFileInfoList();
        for (int i = 0; i < list.length(); i++)
        {
            // set source file in archive
            QString filePath = list.at(i).name;
            QuaZipFile zFile(zip.getZipName(), filePath);
            // open the source file
            zFile.open( QIODevice::ReadOnly );
            // create a bytes array and write the file data into it
            QByteArray ba = zFile.readAll();
            // close the source file
            zFile.close();
            //cодержимое архива нужно поместить в папку extractDir
            QString newFilePath = QString(extractDir.dirName() + "/" + filePath);
            QFileInfo fi(newFilePath);
            QDir dir;
            dir.mkdir(fi.absoluteDir().path());
            // set destination file
            QFile dstFile(newFilePath);
            // open the destination file
            dstFile.open(QIODevice::WriteOnly);
            // write the data from the bytes array into the destination file
            dstFile.write(ba);
            //close the destination file
            dstFile.close();
        }



    }
    //скопировать содержимое папки content в распакованном архиве в папку ContentsSvg в корне приложения
    QDir dir;
    dir.mkdir("ProcessingQueue");
    dir.cd("ProcessingQueue");
    dir.mkdir(destinationName);
    dir.cd(destinationName);

    foreach (QFileInfo fileinfo, extractDir.entryInfoList())
    {
        QFile::copy(fileinfo.filePath(),QString(dir.absolutePath() + "/" + fileinfo.fileName()));
    }
    //потом удалить папку extractDir
    extractDir.removeRecursively();
    zip.close();

    QFile::remove(zipName);


}

void MainWindow::on_closeButton_clicked()
{
    m_ptcpServer->close();
    close();
}


