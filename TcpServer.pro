#-------------------------------------------------
#
# Project created by QtCreator 2017-02-07T12:50:04
#
#-------------------------------------------------

QT       += core network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = TcpServer
TEMPLATE = app

DEFINES += QUAZIP_STATIC
INCLUDEPATH += $$PWD/../quazip
DEPENDPATH += $$PWD/../quazip
INCLUDEPATH += $$PWD/../QtZlib


SOURCES += main.cpp\
        mainwindow.cpp \
    quazip/JlCompress.cpp \
    quazip/qioapi.cpp \
    quazip/quaadler32.cpp \
    quazip/quacrc32.cpp \
    quazip/quagzipfile.cpp \
    quazip/quaziodevice.cpp \
    quazip/quazip.cpp \
    quazip/quazipdir.cpp \
    quazip/quazipfile.cpp \
    quazip/quazipfileinfo.cpp \
    quazip/quazipnewinfo.cpp \
    QtZlib/adler32.c \
    QtZlib/compress.c \
    QtZlib/crc32.c \
    QtZlib/deflate.c \
    QtZlib/gzclose.c \
    QtZlib/gzlib.c \
    QtZlib/gzread.c \
    QtZlib/gzwrite.c \
    QtZlib/infback.c \
    QtZlib/inffast.c \
    QtZlib/inflate.c \
    QtZlib/inftrees.c \
    QtZlib/trees.c \
    QtZlib/uncompr.c \
    QtZlib/zutil.c \
    quazip/unzip.c \
    quazip/zip.c

HEADERS  += mainwindow.h \
    QtZlib/crc32.h \
    QtZlib/deflate.h \
    QtZlib/gzguts.h \
    QtZlib/inffast.h \
    QtZlib/inffixed.h \
    QtZlib/inflate.h \
    QtZlib/inftrees.h \
    QtZlib/trees.h \
    QtZlib/zconf.h \
    QtZlib/zlib.h \
    QtZlib/zutil.h \
    quazip/crypt.h \
    quazip/ioapi.h \
    quazip/JlCompress.h \
    quazip/quaadler32.h \
    quazip/quachecksum32.h \
    quazip/quacrc32.h \
    quazip/quagzipfile.h \
    quazip/quaziodevice.h \
    quazip/quazip.h \
    quazip/quazip_global.h \
    quazip/quazipdir.h \
    quazip/quazipfile.h \
    quazip/quazipfileinfo.h \
    quazip/quazipnewinfo.h \
    quazip/unzip.h \
    quazip/zip.h

FORMS    += mainwindow.ui
